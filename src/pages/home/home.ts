import { Component } from '@angular/core';
import { InAppBrowser } from 'ionic-native';
import { SocialSharing } from 'ionic-native';
import { NavController, NavParams, LoadingController, AlertController, ActionSheetController, ToastController } from 'ionic-angular';
import { LocalStorage } from '../../providers/local-storage';
import { Storage } from '@ionic/storage';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

import { FeedService } from '../../providers/feeds';
import 'rxjs/add/operator/debounceTime';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
    providers: [FeedService]

})
export class HomePage {
  
private items: any;
private title: any;
private searchQuery: string = '';
itemsRetreived: any;
  constructor(
    private http: Http,
    public storage: Storage,
public localStorage: LocalStorage,
    private nav: NavController,
    public navParams: NavParams,
    private feedService: FeedService,
    private loadCtrl: LoadingController,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private actionCtrl: ActionSheetController) {
      let loadingPopup = this.loadCtrl.create({
      content: 'Loading posts...'
    });
 
    this.loadFeed();
loadingPopup.dismiss();
       

  }
   loadFeed() {
    this.feedService.loadall()
      .subscribe(data => {
        this.items = data.items;
        this.itemsRetreived =this.items;
        console.log(data);

        //  });
      });
  }
  doRefresh(refresher) {
              this.loadFeed();

    console.log('Begin async operation', refresher);

    setTimeout(() => {

      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }
initializeItems() {
    this.items = this.title;
  }
public openPage(url: string) {
let browser = new InAppBrowser(url, '_blank');
// window.open(url, '_blank' );
}

getItems(ev: any) {
    // Reset items back to all of the items
this.items = this.itemsRetreived;
    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() !== '') {
      this.items = this.items.filter((item) => {
        return (item.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
  
   twitterShare(url) {
    // share(message, subject, file, url)
    SocialSharing.shareViaTwitter(url);
  }
  shareViaFacebook(url) {
    // share(message, subject, file, url)
    SocialSharing.shareViaFacebook(url);
  }
  public ionViewWillEnter() {

  }
  
setFav(data){
    console.log('data added '+data);
    this.storage.set('link1', data);
    this.storage.get('link1').then((data) => {
        
    });
  };

}