import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ActionSheetController, ToastController } from 'ionic-angular';
import { FeedService } from '../../providers/feeds';
import { Storage } from '@ionic/storage';
import { InAppBrowser } from 'ionic-native';
import { SocialSharing } from 'ionic-native';
import { LocalStorage } from '../../providers/local-storage';

@Component({
  selector: 'page-electronics',
  templateUrl: 'electronics.html',
  providers: [FeedService]
})
export class ElectronicsPage {
  private items: any;
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  constructor(
public storage: Storage,
public localStorage: LocalStorage,
    private nav: NavController,
    public navParams: NavParams,
    private feedService: FeedService,
    private loadCtrl: LoadingController,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private actionCtrl: ActionSheetController) {
    this.loadFeed();

    

  }

  fav;
  

  loadFeed() {
    this.feedService.loadelet()
      .subscribe(data => {
        this.items = data.items;
        console.log(data);

        //  });
      });
  }

  public openPage(url: string) {
    let browser = new InAppBrowser(url, '_blank');
    // window.open(url, '_blank' );
  }


  twitterShare(url) {
    // share(message, subject, file, url)
    SocialSharing.shareViaTwitter(url);
  }
  shareViaFacebook(url) {
    // share(message, subject, file, url)
    SocialSharing.shareViaFacebook(url);
  }
setFav(){
        this.localStorage.setFav('url');
        console.log('saved data');
}

  public ionViewWillEnter() {

  }
}

