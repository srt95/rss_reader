import { Component } from '@angular/core';
import { NavController,NavParams, LoadingController, AlertController, ActionSheetController,ToastController } from 'ionic-angular';
import { FeedService} from '../../providers/feeds';
//import { Storage } from '@ionic/storage';
import { InAppBrowser } from 'ionic-native';

import { SocialSharing } from 'ionic-native';

@Component({
  selector: 'page-espsubs',
  templateUrl: 'espsubs.html',
  providers: [FeedService]
})
export class EspsubsPage {
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  } 
 public items: any[];

 constructor(
   
    private nav: NavController,
     public navParams: NavParams,
    private feedService: FeedService,
    private loadCtrl: LoadingController,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private actionCtrl: ActionSheetController) {
      this.loadFeed();
    }
loadFeed() {
      this.feedService.loadespsub()
      .subscribe(data => {
        this.items = data.items;
        console.log(data);

    //  });
  });
    }
public openPage(url: string) {
let browser = new InAppBrowser(url, '_blank');
// window.open(url, '_blank' );
}
 
 
  
twitterShare(url){
    // share(message, subject, file, url)
    SocialSharing.shareViaTwitter(url); 
  }
  shareViaFacebook(url){
    // share(message, subject, file, url)
    SocialSharing.shareViaFacebook(url); 
  }
    
   
    public ionViewWillEnter() {
   
    }
}

