import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Component } from '@angular/core';
import { Platform,NavController,NavParams, LoadingController, AlertController, ActionSheetController,ToastController } from 'ionic-angular';
import { FeedService } from '../../providers/feeds';
//import { Storage } from '@ionic/storage';
import { InAppBrowser } from 'ionic-native';
import { SocialSharing } from 'ionic-native';

@Component({
  selector: 'page-technology',
  templateUrl: 'technology.html',
  providers: [FeedService]
})
export class TechnologyPage {
 
items;


 
 constructor(
   public platform: Platform,
    private nav: NavController,
     public navParams: NavParams,
    private feedService: FeedService,
    private loadCtrl: LoadingController,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private actionCtrl: ActionSheetController) {
    this.platform = platform;
    


      this.loadFeed();

    }  
doRefresh(refresher) {
      this.feedService.loadtec()
      .subscribe(data => {
        this.items = data.items;
        if(refresher != 0)
         refresher.complete();
    }); 
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  } 
       

loadFeed() {
      this.feedService.loadtec()
      .subscribe(data => {
        this.items = data.items;
        console.log(data);

    
  });
    }
    
public openPage(url: string) {
let browser = new InAppBrowser(url, '_blank');
// window.open(url, '_blank' );
}

 
   
twitterShare(url){
    // share(message, subject, file, url)
    SocialSharing.shareViaTwitter(url); 
  }
  shareViaFacebook(url){
    // share(message, subject, file, url)
    SocialSharing.shareViaFacebook(url); 
  }
  
}

