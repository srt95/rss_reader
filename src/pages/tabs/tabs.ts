import { Component } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';

//pages
import { HomePage } from '../home/home';
import { MenuPage } from '../menu/menu';
@Component({
  templateUrl: 'tabs.html',
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = HomePage;
  tab2Root: any = MenuPage;
nav:NavController;
  constructor(private navCtrl:NavController,private navParams:NavParams) {
     

  }
 
  setFunc(){
      this.navCtrl.setRoot(TabsPage);
  }
  }

