import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ActionSheetController, ToastController } from 'ionic-angular';
import { FeedService } from '../../providers/feeds';
import { Storage } from '@ionic/storage';
import { InAppBrowser } from 'ionic-native';
import { SocialSharing } from 'ionic-native';
import { LocalStorage } from '../../providers/local-storage';
import 'rxjs/add/operator/map'


@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html',
  providers: [FeedService]

})
export class FavoritesPage {
   private items: any;
  link;
  url;
  fav;
  constructor(
    public storage: Storage,
    public localStorage: LocalStorage,
    private nav: NavController,
    public navParams: NavParams,
    private feedService: FeedService,
    private loadCtrl: LoadingController,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private actionCtrl: ActionSheetController) {
    this.storage.get('link1').then((data) => {
      this.items = data;
      this.fav=this.items;
      console.log(data);
    });
  }
   doRefresh(refresher) {
           
                   this.storage.clear();

    console.log('Begin async operation', refresher);

    setTimeout(() => {

      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }
  public openPage(url: string) {
    let browser = new InAppBrowser(url, '_blank');
    // window.open(url, '_blank' );
  }
  
setFav(data){
    console.log('data added '+data);
    this.storage.set('link1', data);
    this.storage.get('link1').then((data) => {
        
    });
  };
  delFav(index){
    this.items.splice(index,1);
     this.storage.set('link2', JSON.stringify(this.items));
    console.log('deleted favorite item');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritesPage');
  }
ionViewWillLoad(){
 console.log('ionViewWillLoad FavoritesPage');
}
}
