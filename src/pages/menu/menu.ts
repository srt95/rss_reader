import { SliderPage } from '../start/slider/slider';
import { Component } from '@angular/core';
import { NavController, NavParams, App, AlertController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { HomePage } from '../home/home';
import { IndianPage } from '../indian/indian';
import { TechnologyPage } from '../technology/technology';
import { LoginPage } from '../start/login/login';

import { EspPage } from '../esp/esp';
import { NewsPage } from '../news/news';
import { ApPage } from '../ap/ap';
import { AutomationPage } from '../automation/automation';
import { AutosubPage } from '../autosub/autosub';
import { ElectronicsPage } from '../electronics/electronics';
import { EspsubsPage } from '../espsubs/espsubs';
import { FeedListPage } from '../feedlist/feedlist';
import { FavoritesPage } from '../favorites/favorites';


@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html'
})
export class MenuPage {

  constructor(public navCtrl: NavController, private app: App, private alertCtrl: AlertController, public navParams: NavParams) { }

  logout() {
    let alert = this.alertCtrl.create({
      title: 'Confirm Logout',
      message: 'Do you want to logout?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            localStorage.setItem("token", "");
            console.log(localStorage.getItem("token"));
            //window.location.reload();
            this.app.getRootNav().setRoot(SliderPage);
            //console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
  }
  tabs() {
    this.navCtrl.push(TabsPage);
  }
  fav() {
    this.navCtrl.push( FavoritesPage);
  }
  apnew() {
    this.navCtrl.push(ApPage);
  }
  news() {
    this.navCtrl.push(NewsPage);
  }
  auto() {
    this.navCtrl.push(AutomationPage);
  }
  autosub() {
    this.navCtrl.push(AutosubPage);
  }
  tec() {
    this.navCtrl.push(TechnologyPage);
  }
  esp() {
    this.navCtrl.push(EspPage);
  }
  espsub() {
    this.navCtrl.push(EspsubsPage);
  }
  elec() {
    this.navCtrl.push(ElectronicsPage);
  }
  all() {
    this.navCtrl.push(FeedListPage);
  }
  indtec() {
    this.navCtrl.push(IndianPage);
  }
  indauto() {
    this.navCtrl.push(IndianPage);
  }
}
