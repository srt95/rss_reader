import { importExpr } from '@angular/compiler/src/output/output_ast';
import { Component } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { NavController, NavParams, AlertController, Platform } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { HttpData } from '../../../providers/httpdata';

import { TabsPage } from '../../tabs/tabs';
import { SignupPage } from '../signup/signup';
import { ForgetpassdataPage } from '../forgetpassdata/forgetpassdata';



@Component({
  selector: 'page-login',
  templateUrl: 'login.html',

})
export class LoginPage {
  registerCredentials = { email: '', password: '' };



  email;
  name;
  token: string;
  url: string;
  baseUrl: string;
  opt1: RequestOptions;
  headers;
  myHeaders: Headers = new Headers;

  constructor(
    public navCtrl: NavController,
    private http: Http,
    private httpData: HttpData,
    public alertCtrl: AlertController
  ) { }



  formInput = { email: '', pass: '' };
  em: string;
  pass: string;
  data;


  getLogin() {

    this.em = this.formInput.email;
    this.pass = this.formInput.pass;

    let headers = new Headers({ 'Content-Type': 'application/json' });

    let userData = {
      "email": this.em,
      "password": this.pass
    };
    this.httpData.userLogin(userData)
      .subscribe(res => {

        // this.token = data.token;
        //// console.log(JSON.parse(this.localService.getUser()));

        this.navCtrl.push(TabsPage);
      }, error => {
        let alert1 = this.alertCtrl.create({
          title: 'Login Failed',
          subTitle: 'Please Verify Username and Password!',
          buttons: ['OK']
        });
        alert1.present();
        console.log(JSON.stringify(error.json()));
      });


  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  go2SignUp() {
    this.navCtrl.push(SignupPage);
  }
  go2ForgotPass() {
    this.navCtrl.push(ForgetpassdataPage);
  }
}