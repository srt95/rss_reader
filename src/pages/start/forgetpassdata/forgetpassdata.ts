import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-forgetpassdata',
  templateUrl: 'forgetpassdata.html'
})
export class ForgetpassdataPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgetpassdataPage');
  }

}
