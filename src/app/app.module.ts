import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { FeedService } from '../providers/feeds';
import { Data } from '../providers/data';

import { IonicStorageModule } from '@ionic/storage';
import { IndianPage } from '../pages/indian/indian';
import { EspPage } from '../pages/esp/esp';
import { NewsPage } from '../pages/news/news';
import { ApPage } from '../pages/ap/ap';
import { AutomationPage } from '../pages/automation/automation';
import { AutosubPage } from '../pages/autosub/autosub';
import { ElectronicsPage } from '../pages/electronics/electronics';
import { EspsubsPage } from '../pages/espsubs/espsubs';
import { FeedListPage } from '../pages/feedlist/feedlist';
import { TabsPage } from '../pages/tabs/tabs';
import { TechnologyPage } from '../pages/technology/technology';
import { HttpData } from '../providers/httpdata';
import { MenuPage } from '../pages/menu/menu';
import { LoginPage } from '../pages/start/login/login';
import { SliderPage } from '../pages/start/slider/slider';
import { SignupPage } from '../pages/start/signup/signup';
import { ForgetpassPage } from '../pages/start/forgetpass/forgetpass';
import { ForgetpassdataPage } from '../pages/start/forgetpassdata/forgetpassdata';
import { FavoritesPage } from '../pages/favorites/favorites';
import { AngularFireModule } from 'angularfire2';

import { LocalStorage } from '../providers/local-storage';
export const firebaseConfig = {
  apiKey: "AIzaSyApxOPJQJ6ViYRcchBIcoNv8t_Wlt3xhig",
    authDomain: "reader-af221.firebaseapp.com",
    databaseURL: "https://reader-af221.firebaseio.com",
    storageBucket: "reader-af221.appspot.com",
    messagingSenderId: "32510954784"
};
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    TabsPage,
    EspsubsPage,
    ElectronicsPage,
    AutosubPage,
    AutomationPage,
    FeedListPage,
    ApPage,
    MenuPage,
    NewsPage,
    EspPage,
    IndianPage,
    TechnologyPage,
    SliderPage,
    SignupPage,
    ForgetpassPage,
    ForgetpassdataPage,
    FavoritesPage

  ],
  imports: [
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig)

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    MenuPage,
    FeedListPage,
    EspsubsPage,
    ElectronicsPage,
    AutosubPage,
    AutomationPage,
    LoginPage,
    ApPage,
     SignupPage,
    ForgetpassPage,
    ForgetpassdataPage,
    NewsPage,
    EspPage,
    IndianPage,
    TechnologyPage,
    SliderPage,
    FavoritesPage
  ],
  providers: [{ provide: ErrorHandler, useClass: IonicErrorHandler }, Data,FeedService,  LocalStorage, HttpData]
})
export class AppModule { }
