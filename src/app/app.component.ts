import { LoginPage } from '../pages/start/login/login';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform,App,AlertController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { TabsPage } from '../pages/tabs/tabs';
import { IndianPage } from '../pages/indian/indian';
import { EspPage } from '../pages/esp/esp';
import { NewsPage } from '../pages/news/news';
import { ApPage } from '../pages/ap/ap';
import { AutomationPage } from '../pages/automation/automation';
import { AutosubPage } from '../pages/autosub/autosub';
import { ElectronicsPage } from '../pages/electronics/electronics';
import { EspsubsPage } from '../pages/espsubs/espsubs';
import { FeedListPage } from '../pages/feedlist/feedlist';
import { SliderPage } from '../pages/start/slider/slider';
import { LocalStorage } from '../providers/local-storage';
import firebase from 'firebase';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = SliderPage;

  pages: Array<{title: string, component: any}>;

  constructor(private local:LocalStorage,public platform: Platform,private app:App,public alertCtrl: AlertController) {
    firebase.initializeApp({
    apiKey: "AIzaSyApxOPJQJ6ViYRcchBIcoNv8t_Wlt3xhig",
    authDomain: "reader-af221.firebaseapp.com",
    databaseURL: "https://reader-af221.firebaseio.com",
    storageBucket: "reader-af221.appspot.com",
    messagingSenderId: "32510954784"
  });
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
     { title: 'Home', component: TabsPage },

      { title: 'Ap News ', component: ApPage },
       { title: 'Indian Automation ', component: IndianPage },
        { title: 'Automation ', component: AutomationPage },
         { title: 'Automation Sub ', component: AutosubPage },
          { title: 'Electornics ', component: ElectronicsPage },
           { title: 'ESP sub ', component: EspsubsPage },
            { title: 'ESP ', component: EspPage },
             { title: 'NewsPage ', component: NewsPage },
             { title: 'All Articles ', component: FeedListPage },
               { title: 'Indian Tech & Startups ', component: IndianPage },

    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
      if (this.local.getToken() == "" || this.local.getToken() == "undefined" || this.local.getToken() == "null" || this.local.getToken() == null) this.rootPage = SliderPage;
            else this.rootPage = TabsPage;
    });
  }


  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
