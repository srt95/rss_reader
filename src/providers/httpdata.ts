import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';


@Injectable()
export class HttpData {
  token: string;
  url: string;
  baseUrl: string;

  headers;
  myHeaders: Headers = new Headers;
  public API_ENDPOINT: string;
  constructor(public http: Http) {
    this.API_ENDPOINT = 'http://staging.feturtles.com:3002/';
    this.baseUrl = 'http://staging.feturtles.com:3002/api/v1/';
    this.myHeaders.append('Content-type', 'application/json');

  }
  public userLogin(data: any) {

    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    let loginURL = this.API_ENDPOINT + 'user/authenticate';
    return this.http.post(loginURL, data, { headers: this.headers }).map((response: Response) => response.json(), JSON.stringify(data));
  }
}