import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class LocalStorage {




    setUser(user:any){
        localStorage.setItem('user',user);
                    console.log(user);

    }
    getUser(){
        return localStorage.getItem('user');
    }
    getToken(){
        return localStorage.getItem('token');
    }

    setToken(token:string){
        localStorage.setItem('token',token);
    }
    setFav(url:any){
        localStorage.setItem('url',url);
    }
    getFav(url:any){
        return localStorage.getItem('url');
    }
  constructor(public http: Http) {
  }


}
