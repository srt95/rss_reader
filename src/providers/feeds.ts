import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { LocalStorage } from '../providers/local-storage';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';




@Injectable()

export class FeedService {
  
  items: any;

  constructor(public http: Http, public storage: Storage,private localStorage: LocalStorage) {
   
  }

  

  loadtec() {
  
    return this.http.get('https://api.rss2json.com/v1/api.json?rss_url=http%3A%2F%2Fwww.inoreader.com%2Fstream%2Fuser%2F1006073747%2Ftag%2FTechnology&api_key=lxu2gr35gfllboozmambvrzqe4cfccietbh0jab5&count=150')
      .map(res => res.json());
      
    }

  loadind() {

    return this.http.get('https://api.rss2json.com/v1/api.json?rss_url=http%3A%2F%2Fwww.inoreader.com%2Fstream%2Fuser%2F1006073747%2Ftag%2FIndian%2520Tech%2520%2526%2520Startup%2527s')
      .map(res => res.json());
  }
  loadesp() {

    return this.http.get('https://api.rss2json.com/v1/api.json?rss_url=http%3A%2F%2Fwww.inoreader.com%2Fstream%2Fuser%2F1006073747%2Ftag%2FESP8266')
      .map(res => res.json());
  }

  loadnews() {

    return this.http.get('https://api.rss2json.com/v1/api.json?rss_url=http%3A%2F%2Fwww.inoreader.com%2Fstream%2Fuser%2F1006073747%2Ftag%2FNews')
      .map(res => res.json());
  }
  loadap() {

    return this.http.get('https://api.rss2json.com/v1/api.json?rss_url=http%3A%2F%2Ffeeds.feedburner.com%2Fsmarthomeblog')
      .map(res => res.json());
  }

  loadespsub() {

    return this.http.get('https://api.rss2json.com/v1/api.json?rss_url=http%3A%2F%2Fwww.inoreader.com%2Fstream%2Fuser%2F1006073747%2Ftag%2F%255BGlobal%255D%2520esp8266')
      .map(res => res.json());
  }
  loadtin() {

    return this.http.get('http://blog.tindie.com/feed/')
      .map(res => res.json());
  }
  loadall() {
    return this.http.get('https://api.rss2json.com/v1/api.json?rss_url=http%3A%2F%2Fwww.inoreader.com%2Fstream%2Fuser%2F1006073747%2Ftag%2Fall-articles&api_key=lxu2gr35gfllboozmambvrzqe4cfccietbh0jab5&count=200')
      .map(res => res.json());
  }
  loadelet() {

    return this.http.get('https://api.rss2json.com/v1/api.json?rss_url=http%3A%2F%2Fwww.inoreader.com%2Fstream%2Fuser%2F1006073747%2Ftag%2FElectronics')
      .map(res => res.json());
  }
  loadautosub() {

    return this.http.get('https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fwww.domoticaforum.eu%2Ffeed.php&api_key=lxu2gr35gfllboozmambvrzqe4cfccietbh0jab5&count=150')
      .map(res => res.json());
  }

  loadindauto() {

    return this.http.get('https://api.rss2json.com/v1/api.json?rss_url=http%3A%2F%2Fwww.inoreader.com%2Fstream%2Fuser%2F1006073747%2Ftag%2FIndian%2520Tech%2520%2526%2520Startup%2527s')
      .map(res => res.json());
  }

loadFav(){
  return this.localStorage.getFav('res');
}
}
